+++
title = "Jackson Mills - Resume"
+++

## :tada: Jackson Mills <sup>(they/them)</sup>

{{< figure class="avatar" src="/avatar.png" alt="avatar">}}

Software Engineer and Artist

Melbourne, VIC

[Gitlab](https://gitlab.com/jacksonm)

---

Hi, I'm Jackson!
<br />
Highly accomplished and with over 9 years of experience in backend engineering, I aim to bring innovative and ethical 
solutions to difficult problems. 
<br /><br />

My programming journey started way back in 2011 when my family PC couldn't run Team Fortress 2.
Not being one to give up easily, I downloaded `notepad++` and embarked on writing my own graphics config - 
did my masterpiece render the UI completely unusable? Yes. *But I was hooked.*
<br />

> Interested in seeing what makes this resume tick? Check out the [repo!](https://gitlab.com/jacksonm/hugo-resume)

Fast-forward a few years I've had the privilege of working on sophisticated BigData pipelines ingesting
and analyzing over 2% of all Australia's utilities usage data, a first-of-it's-kind online learning platform,
a drag-and-drop scripting language, an ORM, countless micro-services, 
and even taught some kids to love the code just like I did.
<br /><br />

My personal software philosophies are 'the backend is best done boring', 'good code is simple code', and
'you should understand why you’re doing something before you understand how'. <br />
I thrive in agile organisations where I'm not constrained to a single role or responsibility. <br />
I believe in empowering and unblocking engineers to do their best work. <br />
I aim to always have a multiplicative effect on those around me. <br />
And I feel right at home diving into domain or stack I've never worked with before.
<br /><br />

I aim to advocate for both my fellow engineers and engineering excellence in the work place. 
I am not afraid to apply a critical lense to the ways my team or organization works and champion initiatives
to improve the quality of the work we produce and the lived experience of my fellow employees. 
<br /><br />

Outside the office, I am always looking for innovative ways to utilize my technical expertise.
Right now I am exploring the worlds of algorithmic trading, computer vision, and signal processing.
My personal dream is to one day own my own exhibition space to showcase multisensory experiences
where the worlds of art, music and technology intersect.
<br /><br />

*Think I'd be a good fit for your team? Drop me a line and let's have a chat.*

---

## Experience

### SNR. SOFTWARE ENGINEER
##### :bulb: 'Flo Energy'

*April 2024 - PRESENT*

 * As the *founding engineer* at Flo's Australian office, adapted and deployed the previously locale-specific
   application to the Australian market. 
 * Designed, implemented and educated engineering staff on an entire suite of database and system
   design patterns to enable the transition from a Singapore-specific data model and application to
   a market agnostic one, enabling the rollout to Australia *and* following international markets.
 * Lead a number of initiative to transition the business from the start-up to scale-up phase:
   * Transitioning from a monolithic application to team-owned microservice.
   * Introduced a suite of Core Technology Principle to guide decision-making, 
     as well as a formal design review process.
   * Created a set of technical and behavioural best practices; ranging from testing, code style, git usage,
     API design, service ownership and more.
     <br /><br />

### SNR. SOFTWARE ENGINEER/SYSTEMS ARCHITECT
##### :city_sunrise: 'Bueno Analytics' - Research and Development Team

*July 2021 - March 2024*

 * Developed driver functionality deployed to customers device-to-enterprise edge devices (Tridium's Niagara framework).
   This functionality automated a manual data tagging process 
   saving 1.6 millions dollars annually in operational costs. 
 * Worked as the lead systems architect on a number of interconnected projects
   replacing legacy systems. These new projects had to replace the legacy systems 
   in-flight while simultaneously delivering new functionality and improving throughput by magnitudes.
 * Architected and developed a fully distributed data ingestion platform to handle millions of daily records
 * Architected and developed an event-based, ML-driven analytics engine.
 * Worked with subject-matter experts, customer success advocates and
   external teams to scope and document project requirements.
   <br /><br />
   
   
### SNR. SOFTWARE ENGINEER
##### :construction: 'Bueno Analytics' - Platform Team

*May 2021 - June 2022*

 * Continually analyzed and improved the existing architecture. 
   Regularly reviewed and optimized the data model, API contracts, ETL pipelines, as well as
   engineering and business processes.
 * Worked with team leads to break down and plan complex features.
   Reduced user stories to actionable items;
   further transformed these items into atomic feature releases to provide the most value to the end user
 * Analyzed business critical modules that were under-performing.
   Optimized SQL and Hibernate queries, as well as entity relationships 
   to facilitate the development of previous unfeasible features.
 * Expanded internal tooling including 
   automated load testing of key API's, and a mature end-to-end testing suite
 * Documented and rolled out a set of best practices regarding software engineering and 
   continuous integration and delivery to standardize code and deployments.
 * Managed stakeholders and built trusting relationships with internal and external users
 * Conducted regular pairing and mentor sessions. 
   Introduced the team to modern development practices, paradigms and patterns.
 * Shadowed C-level staff to understand the challenges the business and customers were facing.
  This responsibility included acting as a go-to source of technical expertise and an advocate
  for the engineering teams for the executive staff.
   <br /><br />


### SOFTWARE ENGINEER
##### :seedling: 'Bueno Analytics' - Product Team

*Nov 2019 - May 2021* 

 * Worked as a backend developer on an environmental sustainability analytics platform 
   comprised of over 15 microservices (`kotlin`, `python`, `typescript`, `java`). 
   'Owned' several services, being responsible for their up-keep and feature planning.
 * Owned the entire software lifecycle. 
   Developed CI/CD pipelines to test, release and deploy services to a number of environments.
 * Developed IaC modules to manage key pieces of infrastructure. 
   Championed migrating traditionally deployed infrastructure to pulumi-managed resources.
 * Worked directly with front end and data science teams to plan and deliver new platform features.
   <br /><br />


### DEVELOPER
##### :books: 'ISH Group'

*Feb 2016 - Oct 2019*

 * Worked as a fullstack developer on a student management system 
   used by a number of Australia's largest universities and private colleges.
 * Responsible for developing integrations between application and external APIs.
 * Assisted in developing a drag-and-drop scripting language to empower uses to create their own automations.
   This process included designing a deterministic grammar and implementing it as code.
 * Worked to migrate user interface from Java Swing to React. 
   Developed new user experiences based on the Material Design specifications
 * Regularly met with enterprise clients and key stakeholder to discuss their needs and develop bespoke solutions
 * Collaborated with a distributed team daily. 
   Managed and priorities tasks between a team spanning three countries
   <br /><br />


### BUSINESS OWNER AND TUTOR
##### :computer: 'Coding 4 Kids'

*May 2015 - Feb 2017*

 * Developed and delivered a suite of programming courses aimed at children aged 8-14
 * Managed and mentored a small team of tutors
 * Collaborated with local high schools and community colleges 
   to offer these courses as part of their extracurricular activities
   <br /><br />
   

### ADMINISTRATIVE OFFICER
##### :bar_chart: 'St Geroge & Sutherland Community College'

*Apr 2012 - Feb 2017*

 * Worked to improve business practices and implement more efficient business process
 * Automated many previously manual administrative tasks
   <br /><br />

---

## Core skills
### TECHNICAL PROFICIENCIES
 * Kotlin, Java, Go
 * Python, Typescript
 * Spring-boot, JPA, Hibernate
 * Postgres, TimescaleDB, database design, data modeling
 * Distributed systems, data streaming, event-driven architecture
 * IaC, CI/CD pipelines (Pulumi, Gitlab CI)
 * GCP, AWS

### SOFT SKILLS
 * Organisational efficiency 
 * Systems architecture
 * Project planning 
 * Cross-functional and multidisciplinary collaboration
 * Mentorship
 * Engineer and worker advocacy

---

## EDUCATION
Bachelor of Computer Science

University of Sydney

*2016 to 2019*

---

## NOTABLE PERSONAL PROJECTS
 * `vidproc` - Computer vision and video manipulation generative art project
 * `disgo` - Distributed redis cluster client (Go)
 * `buen-os` - Bootable OS (Rust) 
 * `kotlin-starter`/`go-starter` - Template projects to quickly create a new microservice with supporting datastore (Kotlin/Go)
 * `ozflogs` - log parser and aggregation platform (Python)

---

## References

Available on request
