# Jackson Mills - Resume

This is my resume. It is built on the static site generation engine Hugo. 
It uses Gitlab Pages to deploy and host the site. 

See it in action here: [https://jacksonm.gitlab.io](https://jacksonm.gitlab.io/hugo-resume/about) 

### Tech

* [Go](https://golang.org/)
* [Hugo](https://gohugo.io/)
* [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/)
* [Researcher](https://themes.gohugo.io/themes/hugo-researcher/)

### Usage

Starting hugo server for local develop:
```bash
hugo server -D
```

Building static pages:
```bash
hugo -D
```
